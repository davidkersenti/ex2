import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users = [
   {name:'Yotam Halperin',email:'yotam10halperin@gmail.com'},
    {name:'Loir Elyaho',email:'lior8elyaho@gmail.com'},
    {name:'Bar Timor',email:'bar11timor@yahoo.com'}
  ]

  constructor() { }

  ngOnInit() {
  }

}
